import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TableComponent } from './components/table/table.component';
import { PaginatorComponent } from './components/paginator/paginator.component';

import { DataService } from './services/data.service';
import { PagerService } from './services/pager.service';
import { SearchFilterPipe } from './pipes/search-filter.pipe';
import { SortFilterPipe } from './pipes/sort-filter.pipe';
@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    PaginatorComponent,
    SearchFilterPipe,
    SortFilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
      DataService,
      PagerService,
      SearchFilterPipe,
      SortFilterPipe
  ],
  exports: [
      SearchFilterPipe,
      SortFilterPipe
],
  bootstrap: [AppComponent]
})
export class AppModule { }
