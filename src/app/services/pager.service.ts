export class PagerService {
    getPager(totalItems: number, currentPage, pageSize, paginationItemSize) {
        // calculate total pages
        const totalPages = Math.ceil(totalItems / pageSize);
        // tslint:disable-next-line:one-variable-per-declaration
        let startPage: number, endPage: number;
        if (paginationItemSize === 10) {
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }
        } else if (paginationItemSize === 5) {
            if (totalPages <= 5) {
                // less than 5 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                if (currentPage <= 3) {
                    startPage = 1;
                    endPage = 5;
                } else if (currentPage + 1 >= totalPages) {
                    startPage = totalPages - 4;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 2;
                    endPage = currentPage + 2;
                }
            }
        }

        // calculate start and end item index
        const startIndex = (currentPage - 1) * pageSize;
        const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        const pages = this.range(startPage, endPage, 1);

        // return object with all pager properties required by the view
        return {
            totalItems,
            currentPage,
            pageSize,
            totalPages,
            startPage,
            endPage,
            startIndex,
            endIndex,
            pages
        };
    }

    range(from: number, to: number, step: number): number[] {
        return Array.from(
            ({ length: Math.floor((to - from) / step) + 1 }),
            (v, k) => from + k * step
        );
    }
}
