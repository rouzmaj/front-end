import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
    providedIn: 'root'
})
export class DataService {
    url = environment.baseUrl;
    jwt = environment.token;

    constructor(private http: HttpClient) { }

    getData() {
        return this.http.get(this.url + 'qpl?offset=0&pageSize=100', {
            headers: { 'Content-Type': 'application/json', Authorization: 'Bearer' + this.jwt }
        });
    }
}
