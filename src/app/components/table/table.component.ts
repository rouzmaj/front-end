import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { PagerService } from '../../services/pager.service';
import { SearchFilterPipe } from '../../pipes/search-filter.pipe';
import { SortFilterPipe } from '../../pipes/sort-filter.pipe';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
    sorkKey: string;
    isAsc: boolean;
    visibleList: any;
    filterKeys: any = {};
    TotalCount: number;
    ItemsPerPage = 25;
    page = 1;
    pageSize: Array<number> = [20 , 40, 60, 80, 100 ];
    count  = 0;
    pager: any = {};
    leftColspan: number;
    rightColspan: number;
    data: any;

  constructor(
      private dataService: DataService,
      private pagerService: PagerService,
      private searchFilter: SearchFilterPipe,
      private sortFilter: SortFilterPipe
  ) { }

  ngOnInit() {
    this.getTableData();
  }
  getTableData() {
    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.getData().subscribe((data) => {
      this.data = data;
      this.generateTableData();
    });
  }

  generateTableData() {
    for (const [key] of Object.entries(this.data[0])) {
      this.filterKeys[key] = '';
    }
    this.visibleList = this.searchFilter.transform(this.data, this.filterKeys);
    this.count = this.visibleList.length;
    this.TotalCount = this.data.length;
    this.setHeader();
  }

  setHeader() {
    const columncount = Object.keys.length;
    this.leftColspan = (columncount) % 2 === 0 ? (columncount) / 2 : Math.floor(columncount / 2);
    this.rightColspan = (columncount) % 2 === 0 ? (columncount) / 2 : Math.round(columncount / 2);
  }
  sortNull() {}

  search(value, item) {
    this.filterKeys[item] = value;
    this.visibleList = this.searchFilter.transform(this.data, this.filterKeys);
    this.count = this.visibleList.length;
  }

  sort(key: string) {
    this.isAsc = !this.isAsc;
    this.sorkKey = key;
    this.visibleList = this.sortFilter.transform(this.visibleList, this.sorkKey, this.isAsc);
  }
  changePageSize(item) {
    this.ItemsPerPage = item;
    this.pager = this.pagerService.getPager(this.data.length, 1, this.ItemsPerPage, 5);
    this.visibleList = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  changePagination(pager) {
    this.pager = pager;
    this.visibleList = this.data.slice(pager.startIndex, pager.endIndex + 1);
  }

}
